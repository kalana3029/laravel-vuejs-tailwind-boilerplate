<?php

namespace App;

use \DateTimeInterface;
use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\SuperAdminScope;

class Role extends Model {

    use HasAdvancedFilter;
    use SoftDeletes;

    public $table = 'roles';
    protected $orderable = [
        'id',
        'title',
    ];
    protected $filterable = [
        'id',
        'title',
        'permissions.title',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    protected $fillable = [
        'title',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
   
    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

     /**
     * Scope a query to only include popular users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutSuperAdmin($query)
    {
        return $query->where('id', '!=', 1);
    }
}
