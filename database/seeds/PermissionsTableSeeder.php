<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder {

    public function run() {
        
        Permission::updateOrCreate(['title' => 'role_access'],['id' => 1]);
        Permission::updateOrCreate(['title' => 'role_create'],['id' => 2]);
        Permission::updateOrCreate(['title' => 'role_edit'],['id' => 3]);
        Permission::updateOrCreate(['title' => 'role_delete'],['id' => 4]);
        Permission::updateOrCreate(['title' => 'user_access'], ['id' => 5]);
        Permission::updateOrCreate(['title' => 'user_create'],['id' => 6]);
        Permission::updateOrCreate(['title' => 'user_edit'],['id' => 7]);
        Permission::updateOrCreate(['title' => 'user_delete'],['id' => 8]);
    }

}
