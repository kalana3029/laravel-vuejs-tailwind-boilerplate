<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        User::upsert(
                [
                    'name' => 'Super Admin',
                    'email' => 'super.admin@admin.com',
                    'password' => Hash::make('super@admin'),
                    'remember_token' => null,
                ], ['id' => 1]);
    }

}
