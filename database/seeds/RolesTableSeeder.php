<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder {

    public function run() {
        Role::upsert(['title' => 'Super Admin'],['id' => 1],);
        Role::upsert(['title' => 'Admin'],['id' => 2]);
        Role::upsert(['title' => 'User'],['id' => 3]);
    }

}
