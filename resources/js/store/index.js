import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// Load store modules dynamically.
const requireContext = require.context('./modules', false, /.*\.js$/)

let modules = requireContext.keys()
  .map(file =>
    [file.replace(/(^.\/)|(\.js$)/g, ''), requireContext(file)]
  )
  .reduce((modules, [name, module]) => {
    if (module.namespaced === undefined) {
      module.namespaced = true
    }

    return { ...modules, [name]: module }
  }, {})

import UsersIndex from './admin/user/index'
import UsersSingle from './admin/user/single'  
import RolesIndex from './admin/role/index'
import RolesSingle from './admin/role/single'  

modules['UsersIndex'] = UsersIndex;
modules['UsersSingle'] = UsersSingle;
modules['RolesIndex'] = RolesIndex;
modules['RolesSingle'] = RolesSingle;

export default new Vuex.Store({
  modules
})
