import axios from 'axios';
import { serialize } from 'object-to-formdata';

function initialState() {
    return {
      entry: {
        id: null,
        title: '',
        permissions: []
      },
      lists: {
        permissions: []
      },
      loading: false
    }
  }
  
  const route = '/api/admin/roles'
  
  const getters = {
    entry: state => state.entry,
    lists: state => state.lists,
    loading: state => state.loading
  }
  
  const actions = {
    storeData({ commit, state, dispatch }) {
      commit('setLoading', true)
      dispatch('alert/resetState', null, { root: true })
  
      return new Promise((resolve, reject) => {
        let params = serialize(state.entry, {
          indices: true,
          booleansAsIntegers: true
        })
        axios
          .post(route, params)
          .then(response => {
            resolve(response)
            dispatch(
              'alert/setAlert',
              { title: 'Information', message: 'Successfully Created', color: 'success' },
              { root: true }
            )
          })
          .catch(error => {
            let message = error.response.data.message || error.message
            let errors = error.response.data.errors
  
            dispatch(
              'alert/setAlert',
              { title: 'Ops', message: message, errors: errors, color: 'error' },
              { root: true }
            )

            reject(error)
          })
          .finally(() => {
            commit('setLoading', false)
          })
      })
    },
    updateData({ commit, state, dispatch }) {
      commit('setLoading', true)
      dispatch('alert/resetState', null, { root: true })
  
      return new Promise((resolve, reject) => {
        let params = serialize(state.entry, {
          indices: true,
          booleansAsIntegers: true
        })
        params.set('_method', 'PUT')
        axios
          .post(`${route}/${state.entry.id}`, params)
          .then(response => {
            resolve(response)
            dispatch(
              'alert/setAlert',
              { title: 'Information', message: 'Successfully Updated', color: 'success' },
              { root: true }
            )
          })
          .catch(error => {
            let message = error.response.data.message || error.message
            let errors = error.response.data.errors
  
            dispatch(
              'alert/setAlert',
              { title: 'Ops', message: message, errors: errors, color: 'error' },
              { root: true }
            )

            reject(error)
          })
          .finally(() => {
            commit('setLoading', false)
          })
      })
    },
    setTitle({ commit }, value) {
      commit('setTitle', value)
    },
    setPermissions({ commit, state }, value) {
      commit('setPermissions', value)
    },
    fetchCreateData({ commit, dispatch }) {
      commit('setLoading', true)
      dispatch('alert/resetState', null, { root: true })
      axios.get(`${route}/create`).then(response => {
        commit('setLists', response.data.meta)
        commit('setLoading', false)
      })
    },
    fetchEditData({ commit, dispatch }, id) {
      commit('setLoading', true)
      dispatch('alert/resetState', null, { root: true })
      axios.get(`${route}/${id}/edit`).then(response => {
        commit('setEntry', response.data.data)
        commit('setLists', response.data.meta)
        commit('setLoading', false)
      })
    },
    fetchShowData({ commit, dispatch }, id) {
      dispatch('alert/resetState', null, { root: true })
      axios.get(`${route}/${id}`).then(response => {
        commit('setEntry', response.data.data)
      })
    },
    resetState({ commit }) {
      commit('resetState')
    }
  }
  
  const mutations = {
    setEntry(state, entry) {
      state.entry = entry
    },
    setTitle(state, value) {
      state.entry.title = value
    },
    setPermissions(state, value) {
      state.entry.permissions = value
    },
    setLists(state, lists) {
      state.lists = lists
    },
    setLoading(state, loading) {
      state.loading = loading
    },
    resetState(state) {
      state = Object.assign(state, initialState())
    }
  }
  
  export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
  }