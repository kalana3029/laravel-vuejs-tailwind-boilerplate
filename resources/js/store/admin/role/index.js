import axios from 'axios';

const set = key => (state, val) => {
  state[key] = val
}

function initialState() {
    return {
      paginateData: {},
      data: [],
      total: 0,
      query: {},
      loading: false
    }
  }
  
  const route = '/api/admin/roles'
  
  const getters = {
    paginateData: state => state.paginateData,
    data: state => state.data,
    total: state => state.total,
    loading: state => state.loading
  }
  
  const actions = {
    fetchIndexData({ commit, state, dispatch }) {
      commit('setLoading', true)
      dispatch('alert/resetState', null, { root: true })
      axios
        .get(route, { params: state.query })
        .then(response => {
          commit('setPaginateData', response.data)
          commit('setData', response.data.data)
          commit('setTotal', response.data.total)

        })
        .catch(error => {
          message = error.response.data.message || error.message
          // TODO error handling
        })
        .finally(() => {
          commit('setLoading', false)
        })
    },
    destroyData({ commit, state, dispatch }, id) {
      commit('setLoading', true)
      dispatch('alert/resetState', null, { root: true })
      axios
        .delete(`${route}/${id}`)
        .then(response => {
          dispatch(
            'alert/setAlert',
            { title: 'Information', message: 'Successfully Deleted', color: 'success' },
            { root: true }
          )
          dispatch('fetchIndexData')
        })
        .catch(error => {
          message = error.response.data.message || error.message
          dispatch(
            'alert/setAlert',
            { title: 'Ops', message: message, errors: errors, color: 'error' },
            { root: true }
          )
        })
        .finally(() => {
          commit('setLoading', false)
        })
    },
    setQuery({ commit }, value) {
      commit('setQuery', _.cloneDeep(value))
    },
    resetState({ commit }) {
      commit('resetState')
    }
  }
  
  const mutations = {
    setPaginateData: set('paginateData'),
    setData: set('data'),
    setTotal: set('total'),
    setQuery(state, query) {
      query.page = (query.offset + query.limit) / query.limit
      state.query = query
    },
    setLoading: set('loading'),
    resetState(state) {
      Object.assign(state, initialState())
    }
  }
  
  export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
  }