import Swal from 'sweetalert2';
import i18n from '~/plugins/i18n'
// state
export const state = {
  title: 'Info',
  message: null,
  errors: null,
  color: 'success'
}

function initialState() {
  return {
    title: 'Info',
    message: null,
    errors: null,
    color: 'success'
  }
}

export const getters = {
  title: state => state.title,
  message: state => state.message,
  errors: state => state.errors,
  color: state => state.color
}

export const actions = {
  setTitle({ commit }, title) {
    commit('setTitle', title)
  },
  setMessage({ commit }, message) {
    commit('setMessage', message)
  },
  setErrors({ commit }, errors) {
    commit('setErrors', errors)
  },
  setColor({ commit }, color) {
    commit('setColor', color)
  },
  setAlert({ commit }, data) {
    commit('setTitle', data.title || 'Info')
    commit('setMessage', data.message || null)
    commit('setErrors', data.errors || null)
    commit('setColor', data.color || null)

    Swal.fire({
      type: data.color,
      title: data.title,
      text: data.message,
      reverseButtons: true,
      confirmButtonText: i18n.t('ok'),
      cancelButtonText: i18n.t('cancel')
    })

  },
  resetState({ commit }) {
    commit('resetState')
  }
}

export const mutations = {
  setTitle(state, title) {
    state.title = title
  },
  setMessage(state, message) {
    state.message = message
  },
  setErrors(state, errors) {
    state.errors = errors
  },
  setColor(state, color) {
    state.color = color || 'success'
  },
  resetState(state) {
    Object.assign(state, initialState())
  }
}
