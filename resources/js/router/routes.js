function page(path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', name: 'welcome', component: page('auth/login.vue') },

  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },

  {
    path: '/admin/dashboard',
    name: 'admin.dashboard',
    meta: {
      title: "Dashboard",
      nav: 'dashboard'
    },
    component: page('home.vue')
  },
  {
    path: '/admin/users',
    name: 'admin.users',
    meta: {
      title: "User Management",
      nav: 'user-management'
    },
    component: page('admin/user/index.vue')
  },
  {
    path: '/admin/users/create',
    name: 'admin.users.create',
    meta: {
      title: "User Management",
      nav: 'user-management'
    },
    component: page('admin/user/create.vue')
  },
  {
    path: '/admin/users/:id/edit',
    name: 'admin.users.edit',
    meta: {
      title: "User Management",
      nav: 'user-management'
    },
    component: page('admin/user/edit.vue')
  },
  {
    path: '/admin/user_groups',
    name: 'admin.user_groups',
    meta: {
      title: "User Group Management",
      nav: 'user-group-management'
    },
    component: page('admin/role/index.vue')
  },
  {
    path: '/admin/user_groups/create',
    name: 'admin.user_groups.create',
    meta: {
      title: "User Group Management",
      nav: 'user-group-management'
    },
    component: page('admin/role/create.vue')
  },
  {
    path: '/admin/user_groups/:id/edit',
    name: 'admin.user_groups.edit',
    meta: {
      title: "User Group Management",
      nav: 'user-group-management'
    },
    component: page('admin/role/edit.vue')
  },
  {
    path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') }
    ]
  },

  { path: '*', component: page('errors/404.vue') }
]
