import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import _ from 'lodash';
import '~/plugins'
import '~/components'

import { abilitiesPlugin } from '@casl/vue'
import ability from './plugins/ability'

import vSelect from 'vue-select'

import "../assets/styles/tailwind.css";
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

Vue.use(abilitiesPlugin, ability, {
  useGlobalProperties: true
})

Vue.component('v-select', vSelect)

/* eslint-disable no-new */
new Vue({
  ability,
  i18n,
  store,
  router,
  ...App
})
